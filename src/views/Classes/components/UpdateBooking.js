import React, {useState} from 'react';
import {Modal, ModalHeader, ModalBody, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Button} from 'reactstrap';
import {Calendar} from '../../../globalcomponents'

const UpdateBooking = (props) => {

	const classSchedule = props.classSchedule
	const [dropdownOpen, setDropdownOpen] = useState(false);
	const [timeSlot, setTimeSlot] = useState("");

	const toggle = () => {
		setDropdownOpen(!dropdownOpen)
	}

	const handleTimeDropdown = (e) => {
		props.handleTimeChange(e.target.value)
		setTimeSlot(e.target.value)
	}

	const handleCompleteUpdate = () => {
		setTimeSlot("")
		props.handleUpdateBooking()
	}

	const handleCancelUpdateBooking = () => {
		props.handleRefresh();
		props.handleShowUpdateForm();
		setTimeSlot("");
	}

	return (
		<React.Fragment>
			<Modal
				isOpen={props.showUpdateForm}
				toggle={handleCancelUpdateBooking}
			>
				<ModalHeader
					toggle={handleCancelUpdateBooking}
				>
				Update class details
				</ModalHeader>
				<ModalBody
					className="d-flex justify-content-center align-items-center flex-column text-center"
				>
					<h4 className="modal-text">Current Booking Details</h4>
					<div className="update-container">
						<p className="update-text">ClassCode: {classSchedule.classCode}</p>
						<p className="update-text">Date: {classSchedule.date}</p>
						<p className="update-text">Time: {classSchedule.time}</p>
						<p className="update-text">Tutor: {classSchedule.tutor}</p>
						<p className="update-text">Student: {classSchedule.student}</p>
						<p className="update-text">Status: {classSchedule.bookingStatus}</p>
					</div>
					<Calendar 
						handleDateChange={props.handleDateChange}
					/>
					<Dropdown
						isOpen={dropdownOpen}
						toggle={toggle}
						className="py-3"
					>
						<DropdownToggle caret className="dropdown-size">
							{timeSlot === "" ? classSchedule.time : timeSlot}
						</DropdownToggle>
						<DropdownMenu>
							<DropdownItem
								value="8 AM to 10 AM"
								onClick={handleTimeDropdown}
							>8 AM to 10 AM</DropdownItem>
							<DropdownItem
								value="10 AM to 12 PM"
								onClick={handleTimeDropdown}
							>10 AM to 12 PM</DropdownItem>
							<DropdownItem
								value="1 PM to 3 PM"
								onClick={handleTimeDropdown}
							>1 PM to 3 PM</DropdownItem>
							<DropdownItem
								value="3 PM to 5 PM"
								onClick={handleTimeDropdown}
							>3 PM to 5 PM</DropdownItem>
						</DropdownMenu>
					</Dropdown>
					<div className="d-flex justify-content-center my-3">
			        	<Button
			        		// color="success"
			        		className="mx-1 success"
			        		onClick={handleCompleteUpdate}
			        		disabled={props.time==="" || props.date==="" ? true : false}
			        	>
			        		Update Class
			        	</Button>
			        	<Button
							// color="danger"
							className="mx-1 danger"
							onClick={handleCancelUpdateBooking}
						>
							Cancel Update
						</Button>
					</div>
				</ModalBody>
			</Modal>
		</React.Fragment>
	)
}

export default UpdateBooking;