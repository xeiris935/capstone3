import React from 'react';
import {DropdownItem} from 'reactstrap';

const TimeSelection = (props) => {
	const time = props.time;
	const indivTime = time.startTime + " to " + time.endTime
	// let currentHour = new Date().getHours()

	return (
		<DropdownItem
			onClick={()=>props.handleTimeChange(indivTime)}
		>
			{indivTime}
		</DropdownItem>
	)
}

export default TimeSelection;