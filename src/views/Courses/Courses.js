import React, {useEffect, useState} from 'react';
import axios from 'axios';
import {CourseCard, BookingForm} from './components';
// import {Button} from 'reactstrap';
import moment from 'moment';
import {NavigationBar, Footer} from '../Layout'

const Courses = () => {

	// const [user,setUser] = useState("");
	const [student, setStudent] = useState("");
	const [courses, setCourses] = useState([]);
	const [showBookingForm, setShowBookingForm] = useState(false);
	const [course, setCourse] = useState("");
	const [date, setDate] = useState("")
	const [time, setTime] = useState("");

	const handleRefresh = () => {
		setCourse("");
		setTime("");
		setDate("");
	}

	const handleShowBookingForm = () => {
		setShowBookingForm(!showBookingForm);
	}

	const handleClassBooking = (course) => {
		handleShowBookingForm();
		setCourse(course);
		console.log(course);
	}

	const handleDateChange = (selectedDay) => {
		setDate(selectedDay)
		console.log(selectedDay)
	}

	const handleTimeChange = (selectedTime) => {
		setTime(selectedTime)
		console.log(selectedTime)
	}

	const handleAddBooking = () => {
		let classCode = moment(new Date).format("x");

		axios.post('https://warm-everglades-62190.herokuapp.com/addClass',{
			classCode: classCode,
			language: course.language,
			time: time,
			date: date,
			student: student,
			tutor: course.tutor
		}).then(res=>{
			// let newClasses = [...courses]
			// newClasses.push(res.data)
			// setCourses(newClasses)
			// handleShowBookingForm();
			handleRefresh();
			console.log(res.data)
		})
	}

	const handleCancelBooking = () => {
		handleShowBookingForm();
		handleRefresh()
	}

	useEffect(()=>{
		if(sessionStorage.token){
			let user = JSON.parse(sessionStorage.user)
			let studentName = user.firstName + " " + user.lastName
			// setUser(user);
			setStudent(studentName);
		}
		axios.get('https://warm-everglades-62190.herokuapp.com/admin/showCourses').then(res=>{
			setCourses(res.data)
		})
	}, []);

	return(
		<React.Fragment>
			<NavigationBar />
				<div className="courses-container">
					<h1 className="text-center header-text">Courses</h1>
					<div className="container">
						<div className="row justify-content-center">
							{courses.map(course=>
								<CourseCard
									key={course._id}
									course={course}
									handleShowBookingForm={handleShowBookingForm}
									handleClassBooking={handleClassBooking}
									student={student}
								/>
							)}
							
							<BookingForm
								showBookingForm={showBookingForm}
								handleShowBookingForm={handleShowBookingForm}
								course={course}
								handleDateChange={handleDateChange}
								handleTimeChange={handleTimeChange}
								time={time}
								date={date}
								handleCancelBooking={handleCancelBooking}
								handleAddBooking={handleAddBooking}
								student={student}
							/>
						</div>
					</div>
				</div>
			<Footer />
		</React.Fragment>
	)
}

export default Courses;