import React, {useState} from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import axios from 'axios';
import {Button} from 'reactstrap';

const CardDetails = (props) => {

	let course = props.course

	const handlePayAndBookClass = async () => {

		props.handleAddBooking()
		props.handleShowPaymentConfirmation()

		let student = props.student
		let price = course.price*100;
		let description = "Payment for " + course.language + " class from Codebase.com";

		console.log(student)
		console.log(price)
		console.log(description)

		let {token} = await props.stripe.createToken({name:student})

		let response = await axios.post('https://warm-everglades-62190.herokuapp.com/admin/charge', {
			headers: {"Content-type":"text-plain"},
			body: token.id,
			amount: price,
			description: description
		})

		// if(response.ok) console.log("Purchase Complete!");
	}

	const [stripeRequired, setStripeRequired] = useState(false)

	const handleOnChange = (e) => {
		if(e.complete === true){
			setStripeRequired(true)
		} 
	}

	return (
		<div className="checkout">
			<h3 className="text-center">Amount: {course.price} PHP</h3>
        	<p>Would you like to complete the purchase for {course.language} class?</p>
        	<CardElement 
        		onChange={handleOnChange}
        	/>
        	<div className="d-flex justify-content-center my-3">
	        	<Button
	        		// color="success"
	        		className="mx-1 success"
	        		onClick={handlePayAndBookClass}
	        		disabled={props.time==="" || props.date==="" || stripeRequired===false ? true : false}
	        	>

	        		Book Now
	        	</Button>
	        	<Button
					// color="danger"
					className="mx-1 danger"
					onClick={props.handleCancelBooking}
				>
					Cancel
				</Button>
			</div>
      	</div>
	)
}

export default injectStripe(CardDetails);